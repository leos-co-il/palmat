(function($) {
	$('#inputCat').change(function() {
		var location = '';
		$( '#inputCat option:selected').each(function() {
			location = $( this ).data('id');
		});
		var select = $('.form-group').find($('#inputSubCat'));
		$('#inputSubCat').children('option:not(:first)').remove();
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			type: 'post',
			dataType: 'json',
			data: {
				productCat: location,
				action: 'cats_search',
			},
			success: function (data) {

				var options = data.html;
				$.each(options, function (i, item) {
					select.append($('<option>', {
						value: i,
						text : item
					}));
				});
			}
		});
	});

	$( document ).ready(function() {
		$('.catalog-trigger').click(function () {
			$('.sidebar-col').addClass( 'active-cat' );
		});
		$('.close-side').click(function () {
			$('.sidebar-col').removeClass( 'active-cat' );
		});
		$('.pop-product-trigger').click(function () {
			var postName = $(this).data('name');
			var postLink = $(this).data('link');
			$('.hidden-text-input').val(postName);
			$('.hidden-link-input').val(postLink);
			$('.pop-form').addClass('show-popup');
			$('.float-form').addClass('show-float-form');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-form').click(function () {
			$('.pop-form').removeClass('show-popup');
			$('.float-form').removeClass('show-float-form');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-button').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		var accordion = $('#accordion');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.question-title').children('.arrow-top').addClass('arrow-bottom');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.question-title').children('.arrow-top').removeClass('arrow-bottom');
		});
	});


})( jQuery );
