<?php

add_action('wp_ajax_nopriv_ajax_function', 'ajax_function');
add_action('wp_ajax_more_ajax_function', 'ajax_function');

function ajax_function()
{

    if (!wp_verify_nonce($_REQUEST['nonce'], "my_user_vote_nonce")) {
        exit("No naughty business please");
    }

    $result['type'] = "error";
    $result['var'] = 1;

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    } else {
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
    die();
}

add_action('wp_ajax_nopriv_cats_search', 'cats_search');
add_action('wp_ajax_cats_search', 'cats_search');


function cats_search()
{

	$result['type'] = "success";

	$term_id = (isset($_REQUEST['productCat'])) ? $_REQUEST['productCat'] : '';
	$terms = get_term_children( $term_id, 'product_cat');
	$html = [];
	foreach ($terms as $term) {
		$term_name = get_term($term)->name;
		$html[$term] = $term_name;
//		$html = load_template_part('views/partials/card', 'option', [
//			'item' => $term,
//		]);
	}
	$result['html'] = $html;

	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}
