<?php

the_post();
get_header();
$fields = get_fields();
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'product_cat', ['fields' => 'all']);
$term = isset($post_terms['0']) ? $post_terms['0'] : '';
?>

<?php  get_template_part('views/partials/content', 'block_top', [
		'img' => has_post_thumbnail() ? postThumb() : '',
]); ?>
<div class="container-fluid">
	<div class="row justify-content-md-start justify-content-center">
		<div class="col-xl-2 col-md-3">
			<?php get_template_part('views/partials/content', 'sidebar'); ?>
		</div>
		<div class="col-xl-1 d-xl-flex d-none"></div>
		<div class="col-md-8 col-11">
			<?php if ( function_exists('yoast_breadcrumb') ) : ?>
				<div class="row justify-content-start bread-row">
					<div class="col-12">
						<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
					</div>
				</div>
			<?php endif; ?>
			<?php if($term) : ?>
				<div class="row justify-content-center">
					<div class="col-auto mb-3">
						<h2 class="main-title"><?= $term->name; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row">
				<div class="col">
					<h1 class="product-title"><?php the_title(); ?></h1>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col product-page-col">
					<?php if ($fields['articul'] || $fields['product_pdf']) : ?>
						<div class="product-files-wrap">
							<?php if ($fields['articul']) : ?>
								<div class="product-link art-link">
									<?php echo lang_text(['he' => 'מק”ט:', 'en' => 'SKU:'], 'he');
								echo $fields['articul']; ?></div>
							<?php endif;
							if ($fields['product_pdf']) : ?>
								<a class="product-link" href="<?= $fields['product_pdf']['url']; ?>" download>
									<?= lang_text(['he' => 'להורדת שרטוט ב-PDF', 'en' => 'To download a drawing in PDF'], 'he'); ?>
								</a>
							<?php endif; ?>
						</div>
					<?php endif; ?>
					<div class="about-text product-info-block">
						<?php the_content(); ?>
					</div>
					<?php if ($fields['product_colors']) : ?>
						<div class="colors-block">
							<div class="colors-link">
								<?= lang_text(['he' => 'מפתח צבעים', 'en' => 'Colors'], 'he'); ?>
							</div>
							<div class="colors-line">
								<?php foreach ($fields['product_colors'] as $x => $color) : ?>
									<div class="color-item">
										<h3 class="color-number"><?= $x; ?></h3>
										<h4 class="color-name"><?= $color['color_item_title']; ?></h4>
										<?php if ($color['color_item_img']) : ?>
											<img src="<?= $color['color_item_img']['url']; ?>" alt="product-color">
										<?php endif; ?>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					<?php endif; ?>
					<div class="product-files-wrap mt-3">
						<?php if ($fields['articul']) : ?>
							<div class="product-link pop-product-trigger"
								 data-name="<?php the_title(); ?>" data-link="<?php the_permalink(); ?>">
								<?php echo lang_text(['he' => 'לפרטים נוספים', 'en' => 'for further details'], 'he'); ?>
							</div>
						<?php endif;
						if ($term) : ?>
						<a class="product-link to-cat-link" href="<?= get_term_link($term); ?>">
							<?= lang_text(['he' => 'חזרה לקטגוריות', 'en' => 'Back to categories'], 'he'); ?>
						</a>
						<?php endif; ?>
					</div>
				</div>
				<?php if (has_post_thumbnail()) : ?>
					<div class="col-xl-4 prod-img-col">
						<a href="<?= postThumb(); ?>" data-lightbox="product" class="product-photo">
							<img src="<?= postThumb(); ?>" alt="product-photo">
						</a>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<section class="pop-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10 col-md-11 col-12 d-flex justify-content-center">
				<div class="float-form">
					<div class="form-col">
						<span class="close-form">
							<img src="<?= ICONS ?>close.png">
						</span>
						<?php if ($f_title = opt('pop_form_title')) : ?>
							<h2 class="main-title"><?= $f_title; ?></h2>
						<?php endif;
						if ($f_text = opt('pop_form_text')) : ?>
							<h3 class="form-subtitle mb-3"><?= $f_text; ?></h3>
						<?php endif;
						lang_form(['he' => '67', 'en' => '70'], '67') ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>
