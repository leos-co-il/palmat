<?php

$mail = opt('mail');
$facebook = opt('facebook');
$address = opt('address');
$index = opt('index');

?>

<footer>
	<div class="footer-main">
		<div class="container-fluid footer-container-menu">
			<div class="row justify-content-end">
				<div class="col-xl-8 col-10">
					<a id="go-top">
						<img src="<?= ICONS ?>to-top.png" alt="to-top-arrow">
					</a>
					<div class="row justify-content-between align-items-start">
						<div class="col-lg-auto col-md-6 col-12 foo-menu">
							<h3 class="foo-title">
								<?= lang_text(['he' => 'מפת אתר', 'en' => 'Site map'], 'he'); ?>
							</h3>
							<div class="menu-border-top">
								<?php getMenu('footer-menu', '2'); ?>
							</div>
						</div>
						<div class="col-lg-auto col-md-6 foo-menu contacts-footer-menu">
							<h3 class="foo-title">
								<?= lang_text(['he' => 'צור קשר', 'en' => 'Contact us'], 'he'); ?>
							</h3>
							<div class="menu-border-top">
								<ul class="contact-list d-flex flex-column">
									<?php if ($mail) : ?>
										<li>
											<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
												<?= lang_text(['he' => 'אימייל:', 'en' => 'Email:'], 'he'); ?>
												<?= $mail; ?>
											</a>
										</li>
									<?php endif;
									if ($address) : ?>
										<li>
											<a href="https://waze.com/ul?q=<?= $address; ?>"
											   class="contact-info-footer" target="_blank">
												<?= lang_text(['he' => 'כתובת:', 'en' => 'Address: '], 'he'); ?>
												<?= $address; ?>
											</a>
										</li>
									<?php endif;
									if ($index) : ?>
										<li>
									<span>
										<?= lang_text(['he' => 'מיקוד:', 'en' => 'Index: '], 'he'); ?>
										 <?= $index; ?>
									</span>
										</li>
									<?php endif; ?>
								</ul>
							</div>
						</div>
						<?php if ($facebook) : ?>
							<div class="col-lg-auto col-md-6 facebook-widget foo-menu d-flex flex-column">
								<h3 class="foo-title">
									<?= lang_text(['he' => 'עשו לנו לייק', 'en' => 'Like us'], 'he'); ?>
								</h3>
								<div class="menu-border-top">
									<iframe src="https://www.facebook.com/plugins/page.php?href=<?= $facebook; ?>&tabs=timeline&width=300px&height=200px&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId"
											width="250px" height="150px" style="border:none;overflow:hidden" scrolling="no"
											frameborder="0" allowTransparency="true" allow="encrypted-media">
									</iframe>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-1"></div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>
<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>

