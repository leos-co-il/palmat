<?php

get_header();
$query = get_queried_object();
$img = get_field('cat_img', $query);
$products = get_posts([
    'numberposts' => -1,
    'post_type' => 'product',
		'suppress_filters' => false,
		'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'term_id',
            'terms' => $query->term_id,
        )
    )
]);
?>

<?php  get_template_part('views/partials/content', 'block_top', [
	'img' => $img ? $img['url'] : '',
]); ?>
	<div class="container-fluid">
		<div class="row justify-content-md-start justify-content-center">
			<div class="col-xl-2 col-md-3">
				<?php get_template_part('views/partials/content', 'sidebar'); ?>
			</div>
			<div class="col-xl-1 d-xl-flex d-none"></div>
			<div class="col-md-8 col-11">
				<div class="row justify-content-center">
					<div class="col-auto mb-3">
						<h2 class="main-title"><?= $query->name; ?></h2>
					</div>
				</div>
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($products as $x => $post) : ?>
						<div class="col-xl-3 col-lg-4 col-sm-6 col-12 product-col">
							<?php  get_template_part('views/partials/card', 'product', [
								'post' => $post,
							]); ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
