<?php
get_header();
$fields = get_fields();
get_template_part('views/partials/content', 'block_top', [
	'img' => has_post_thumbnail() ? postThumb() : '',
]); ?>
<div class="container-fluid">
	<div class="row justify-content-md-start justify-content-center">
		<div class="col-xl-2 col-3">
			<?php get_template_part('views/partials/content', 'sidebar'); ?>
		</div>
		<div class="col-xl-1 d-xl-flex d-none"></div>
		<div class="col-md-8 col-11">
			<?php
			$s = get_search_query();
			$args = array(
				's' => $s
			);
			$the_query = new WP_Query( $args );
			if ( $the_query->have_posts() ) { ?>
			<h4 class="main-title"><?= esc_html__('תוצאות חיפוש עבור: ','leos');?><?= get_query_var('s') ?></h4>
			<div class="row justify-content-center">
				<?php  while ( $the_query->have_posts() ) { $the_query->the_post(); $hey = 1; ?>
					<div class="col-xl-4 col-md-6 col-12 mb-4 wow fadeInUp" data-wow-delay="0.<?= $hey * 2; ?>s">
						<a class="product-card" href="<?php the_permalink(); ?>">
							<?php if ($art = get_field('articul')) : ?>
								<div class="articul-wrap">
									<div class="articul-link">
										<?= lang_text(['he' => 'מק”ט:', 'en' => 'SKU:'], 'he'); echo $art; ?>
									</div>
								</div>
							<?php endif; ?>
							<div class="post-image" <?php if (has_post_thumbnail()) : ?>
								style="background-image: url('<?= postThumb(); ?>')" <?php endif; ?>>
							</div>
							<div class="post-title"><?php the_title(); ?></div>
							<div class="post-link">
								<?= lang_text(['he' => 'לדף המוצר >', 'en' => 'To the product page>'], 'he'); ?>
							</div>
						</a>
					</div>
				<?php }
				} else{ ?>
					<div class="text-center pt-5">
						<h4 class="base-block-title text-center">
							<?= esc_html__('שום דבר לא נמצא','leos'); ?>
						</h4>
					</div>
					<div class="alert alert-info text-center mt-5">
						<p><?= esc_html__('מצטערים, אך שום דבר לא תאם את קריטריוני החיפוש שלך. אנא נסה שוב עם מילות מפתח שונות.','leos'); ?></p>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>
