<?php
/*
Template Name: אודות
*/

get_header();
$fields = get_fields();
$products = get_posts([
	'numberposts' => -1,
	'post_type' => 'product',
]);

?>

<?php  get_template_part('views/partials/content', 'block_top', [
	'img' => has_post_thumbnail() ? postThumb() : '',
		'about' => true,
]); ?>
<article class="about-body">
	<div class="container-fluid">
		<div class="row justify-content-md-start justify-content-center">
			<div class="col-xl-2 col-3">
				<?php get_template_part('views/partials/content', 'sidebar'); ?>
			</div>
			<div class="col-xl-1 d-xl-flex d-none"></div>
			<div class="col-md-8 col-11">
				<div class="row justify-content-center">
					<div class="col-auto mb-3">
						<h2 class="main-title"><?php the_title(); ?></h2>
					</div>
				</div>
				<div class="row justify-content-center align-items-stretch">
					<div class="col">
						<div class="about-text">
							<?php the_content(); ?>
						</div>
					</div>
					<?php if ($fields['about_img']) : ?>
						<div class="col-lg-4 d-flex flex-column align-items-start">
							<div class="about-img mb-3">
								<img src="<?= $fields['about_img']['url']; ?>" alt="about-company">
							</div>
							<?php if ($fields['about_text_photo']) : ?>
								<h3 class="about-text-photo"><?= $fields['about_text_photo']; ?></h3>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
