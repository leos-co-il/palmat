<?php
/*
Template Name: מוצרים
*/

get_header();
$cats = get_terms([
		'taxonomy' => 'product_cat',
		'hide_empty' => false,
		'parent' => 0,
]);
$query_args = [
		'post_type' => 'product',
		'posts_per_page' => -1,
		'suppress_filters' => false,
];
$_categories = (isset($_GET['prod-cat'])) ? $_GET['prod-cat'] : null;
$_sub_categories = (isset($_GET['prod-sub-cat'])) ? $_GET['prod-sub-cat'] : null;
$_query = (isset($_GET['search-query'])) ? sanitize_text_field($_GET['search-query']) : null;
$sq = (isset($_GET['s'])) ? sanitize_text_field($_GET['s']) : null;
if($_categories || $_sub_categories || $_query || $sq){
	$query_args['tax_query'] = [
			'relation' => 'AND',
			$_categories ? [
					'taxonomy' => 'product_cat',
					'field'    => 'term_id',
					'terms'    => [$_categories],
			] : null,
			$_sub_categories ? [
					'taxonomy' => 'product_cat',
					'field'    => 'term_id',
					'terms'    => [$_sub_categories],
			] : null,
	];
	$query_args['s'] = $sq ? $sq : null;
	$query_args['s'] = $_query ? $_query : null;
}
$products = new WP_Query($query_args);
?>

<?php  get_template_part('views/partials/content', 'block_top', [
	'img' => has_post_thumbnail() ? postThumb() : '',
]); ?>
<div class="container-fluid">
	<div class="row justify-content-md-start justify-content-center">
		<div class="col-xl-2 col-md-3">
			<?php get_template_part('views/partials/content', 'sidebar'); ?>
		</div>
		<div class="col-xl-1 d-xl-flex d-none"></div>
		<div class="col-md-8 col-11">
			<?php if ( function_exists('yoast_breadcrumb') ) : ?>
				<div class="row justify-content-start bread-row">
					<div class="col-12">
						<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center">
				<div class="col-auto mb-3">
					<h2 class="main-title"><?php the_title(); ?></h2>
				</div>
			</div>
			<div class="form-part">
				<form method="get" action="<?php the_permalink(); ?>">
					<div class="row align-items-center">
						<div class="form-group col single-search-col">
							<input type="text" name="search-query" class="form-control" id="inputFree" placeholder="<?= esc_html__(lang_text(['he' => 'חיפוש חופשי', 'en' => 'Free search'], 'he'),'leos'); ?>">
						</div>
						<?php if ($cats): ?>
							<div class="form-group col-xl col-sm-6 col-12 input-line">
								<select id="inputCat" name="prod-cat" class="form-control">
									<option selected disabled>
										<?= esc_html__(lang_text(['he' => 'סינון  לפי קטגוריה ', 'en' => 'Filter by category'], 'he'),'leos'); ?>
									</option>
									<?php foreach($cats as $cat): ?>
										<option value="<?= $cat->term_id ?>" data-id="<?= $cat->term_id ?>">
											<?= $cat->name ?>
										</option>
									<?php endforeach ?>
								</select>
							</div>
						<?php endif; ?>
						<div class="form-group col-xl col-sm-6 col-12 input-line">
							<select id="inputSubCat" name="prod-sub-cat" class="form-control">
								<option selected disabled>
									<?= esc_html__(lang_text(['he' => 'סינון  לפי סדרה', 'en' => 'Filter by series'], 'he'),'leos'); ?>
								</option>
							</select>
						</div>
						<div class="form-group col-xl col-12">
							<button type="submit" class="btn btn-search">
								<?= esc_html__(lang_text(['he' => 'חפש', 'en' => 'Search'], 'he'),'leos') ?>
							</button>
						</div>
					</div>
				</form>
			</div>
			<?php if ($products->have_posts()) : ?>
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($products->posts as $x => $post) : ?>
						<div class="col-xl-3 col-lg-4 col-sm-6 col-12 product-col">
							<?php  get_template_part('views/partials/card', 'product', [
									'post' => $post,
							]); ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
