<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>

<?php  get_template_part('views/partials/content', 'block_top', [
		'img' => has_post_thumbnail() ? postThumb() : '',
]); ?>
<div class="container-fluid mb-5">
	<div class="row justify-content-md-start justify-content-center">
		<div class="col-xl-2 col-md-3">
			<?php get_template_part('views/partials/content', 'sidebar'); ?>
		</div>
		<div class="col-xl-1 d-xl-flex d-none"></div>
		<div class="col-md-8 col-11">
			<?php if ($fields['home_title']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto mb-3">
						<h2 class="main-title"><?= $fields['home_title']; ?></h2>
					</div>
				</div>
			<?php endif;
			if ($fields['home_products']) : ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['home_products'] as $x => $post) : ?>
					<div class="col-xl-3 col-lg-4 col-sm-6 col-12 product-col">
						<?php  get_template_part('views/partials/card', 'product', [
								'post' => $post,
						]); ?>
					</div>
				<?php endforeach; ?>
			</div>
			<?php endif; ?>
		</div>
	</div>
</div>


<?php get_footer(); ?>
