<?php if(isset($args['post']) && $args['post']) :?>
	<a class="product-card" href="<?php the_permalink($args['post']); ?>">
		<?php if ($art = get_field('articul', $args['post'])) : ?>
			<div class="articul-wrap">
				<div class="articul-link">
					<?= lang_text(['he' => 'מק”ט:', 'en' => 'SKU:'], 'he'); echo $art; ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="post-image" <?php if (has_post_thumbnail($args['post'])) : ?>
			style="background-image: url('<?= postThumb($args['post']); ?>')" <?php endif; ?>>
		</div>
		<div class="post-title"><?= $args['post']->post_title; ?></div>
		<div class="post-link">
			<?= lang_text(['he' => 'לדף המוצר >', 'en' => 'To the product page>'], 'he'); ?>
		</div>
	</a>
<?php endif; ?>
