<?php if ($sidebar = opt('sidebar')) : ?>
	<div class="sidebar-col">
		<span class="close-side"><img src="<?= ICONS ?>close.png" alt="close"></span>
		<?php foreach ($sidebar as $item) : ?>
			<div class="category-block">
				<h3 class="cat-block-title"><?= $item['side_subtitle']; ?></h3>
				<?php foreach ($item['sidebar_items'] as $cat) : ?>
					<a href="<?= get_term_link($cat); ?>" class="category-link"
					style="background-color: <?= get_field('color', $cat); ?>">
						<?= $cat->name; ?>
					</a>
				<?php endforeach; ?>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>
