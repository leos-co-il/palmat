<section class="main-block <?= (isset($args['about']) && $args['about']) ? 'mb-0' : ''; ?>"
		<?php if (isset($args['img']) && $args['img']) : ?>
style="background-image: url('<?= $args['img']; ?>')" <?php endif; ?>>
	<div class="main-overlay">
		<?php if (!isset($args['about'])) : ?>
			<img src="<?= IMG ?>top-line.png" class="top-line">
		<?php endif;
		if (isset($args['about']) && $args['about']) : ?>
			<img src="<?= IMG ?>top-line-about.png" class="top-line">
		<?php endif; ?>
	</div>
</section>
