<?php if(isset($args['cat']) && $args['cat']) :?>
	<a class="product-card category-item" href="<?= get_term_link($args['cat']); ?>">
		<div class="post-image" <?php if ($img = get_field('cat_img', $args['cat'])) : ?>
			style="background-image: url('<?= $img['url']; ?>')" <?php endif; ?>>
		</div>
		<div class="post-title"><?= $args['cat']->name; ?></div>
		<div class="post-link">
			<?= lang_text(['he' => 'לקטגוריה >', 'en' => 'To category >'], 'he'); ?>
		</div>
	</a>
<?php endif; ?>
