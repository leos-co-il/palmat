<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$mail = opt('mail');
$address = opt('address');
get_template_part('views/partials/content', 'block_top', [
		'img' => has_post_thumbnail() ? postThumb() : '',
]); ?>
<div class="container-fluid mb-5">
	<div class="row justify-content-md-start justify-content-center">
		<div class="col-xl-2 col-3">
			<?php get_template_part('views/partials/content', 'sidebar'); ?>
		</div>
		<div class="col-xl-1 d-xl-flex d-none"></div>
		<div class="col-lg-8 col-md-9 col-11">
			<div class="row justify-content-center">
				<div class="col-auto mb-3">
					<h2 class="main-title"><?php the_title(); ?></h2>
					<div class="contact-content-page"><?php the_content(); ?></div>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<div class="col-xl-8 col-lg-11 col-12 contact-form-col">
					<?php lang_form(['he' => '68', 'en' => '69'], 'he');
					if ($mail || $address) : ?>
					<ul class="contact-list row align-items-start justify-content-center mt-4">
						<?php if ($mail) : ?>
						<li class="col-sm col-12 contact-list-item">
							<img src="<?= ICONS ?>mail.png" alt="mail-icon">
							<a href="mailto:<?= $mail; ?>" class="contact-info">
								<?= $mail; ?>
							</a>
						</li>
						<?php endif;
						if ($address) : ?>
						<li class="col-sm col-12 contact-list-item">
							<img src="<?= ICONS ?>geo.png" alt="geo-icon">
							<a href="https://waze.com/ul?q=<?= $address; ?>"
							   class="contact-info" target="_blank">
								<?= $address; ?>
							</a>
						</li>
						<?php endif; ?>
					</ul>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>
