<?php

namespace WordPress\Plugin\GalleryManager;

use DOMDocument;

abstract class Core
{
    const
        version = '1.6.56'; # Current release number

    public static
        $base_url, # url to the plugin directory
        $plugin_file, # the main plugin file
        $plugin_folder; # the path to the folder the plugin files contains

    public static function init($plugin_file)
    {
        static::$plugin_file = $plugin_file;
        static::$plugin_folder = DirName(static::$plugin_file);

        register_Activation_Hook(static::$plugin_file, [static::class, 'installPlugin']);
        register_Deactivation_Hook(static::$plugin_file, [static::class, 'uninstallPlugin']);
        add_Action('plugins_loaded', [static::class, 'loadBaseUrl']);
        add_Filter('post_class', [static::class, 'addContentUnitPostClass']);
        add_Filter('body_class', [static::class, 'addTaxonomyBodyClass']);
        add_Filter('wp_get_attachment_link', [static::class, 'filterAttachmentLink'], 10, 6);
        add_Filter('get_the_archive_title', [static::class, 'filterArchiveTitle']);
    }

    public static function installPlugin()
    {
        Taxonomies::updateTaxonomyNames();
        Post_Type::updatePostTypeName();
        Taxonomies::registerTaxonomies();
        Post_Type::registerPostType();
        flush_Rewrite_Rules();
    }

    public static function uninstallPlugin()
    {
        flush_Rewrite_Rules();
    }

    public static function loadBaseURL()
    {
        $absolute_plugin_folder = RealPath(static::$plugin_folder);

        if (StrPos($absolute_plugin_folder, ABSPATH) === 0)
            static::$base_url = site_url() . '/' . SubStr($absolute_plugin_folder, Strlen(ABSPATH));
        else
            static::$base_url = Plugins_Url(BaseName(static::$plugin_folder));

        static::$base_url = Str_Replace("\\", '/', static::$base_url); # Windows Workaround
    }

    public static function addContentUnitPostClass($arr_class)
    {
        setType($arr_class, 'ARRAY');
        $arr_class[] = 'gallery-content-unit';
        return $arr_class;
    }

    public static function addTaxonomyBodyClass($arr_class)
    {
        setType($arr_class, 'ARRAY');
        if (Query::isGalleryTaxonomyArchive())
            $arr_class[] = 'gallery-taxonomy';

        return $arr_class;
    }

    public static function filterAttachmentLink($link, $id, $size, $permalink, $icon, $text)
    {
        static $libxml_is_fine = -1;

        if ($libxml_is_fine < 0)
            $libxml_is_fine = Version_Compare(LIBXML_DOTTED_VERSION, '2.7.8', '>=');

        if (WP_Attachment_Is_Image($id) && $libxml_is_fine) {
            $image = get_Post($id);

            # convert the link in an HTML object
            $html = new DOMDocument('1.0', 'UTF-8');
            $html->loadHTML('<?xml version="1.0" encoding="UTF-8" ?>' . $link, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $html->encoding = 'UTF-8';
            $link = $html->childNodes->item(1);

            # Add link attributes
            if (!$link->hasAttribute('title'))
                $link->setAttribute('title', $image->post_title);

            if (!$link->hasAttribute('data-description'))
                $link->setAttribute('data-description', $image->post_content);

            # convert the link node back to a html string
            $link = $html->saveHTML($link);
        }

        return $link;
    }

    public static function filterArchiveTitle($title)
    {
        if (is_Post_Type_Archive(Post_Type::post_type_name))
            return post_type_archive_title('', false);
        else
            return $title;
    }
}
